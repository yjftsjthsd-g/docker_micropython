# docker_micropython

This repo is for building a container image of the unix port of micropython.
This may be useful for ex. testing code on your main computer rather than having
to develop and/or test right on the microcontroller, which is doable but can be
a pain.


## Use

Interactive use:
```
podman run --rm -ti registry.gitlab.com/yjftsjthsd-g/docker_micropython
```

Running programs:
```
podman run --rm -v $PWD/program.py:/program.py:ro registry.gitlab.com/yjftsjthsd-g/docker_micropython program.py
```


## License

This repo is MIT-licensed (see LICENSE), but note that this is just build
scripts; micropython is under its own licenses.

