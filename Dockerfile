FROM docker.io/library/alpine:latest AS build
RUN apk add alpine-sdk pkgconf libffi-dev git make python3 bsd-compat-headers
# this is an *awful* way to patch around cdefs warning that it's deprecated,
# and micropython treating warnings as errors
RUN sed -i '/#warn/d' /usr/include/sys/cdefs.h

RUN git clone --depth 1 https://github.com/micropython/micropython.git /usr/local/src/micropython

# # https://github.com/micropython/micropython/tree/master/ports/unix
# cd ports/unix
# make submodules
# make deplibs  # not 100% clear whether this should be before or after the main make
# make

WORKDIR /usr/local/src/micropython/ports/unix
RUN make submodules && \
    make deplibs && \
    make

# ./build-standard/micropython

CMD /usr/local/src/micropython/ports/unix/build-standard/micropython

FROM docker.io/library/alpine:latest AS result
RUN apk add libffi
COPY --from=build /usr/local/src/micropython/ports/unix/build-standard/micropython /usr/local/bin/micropython
# make sure the thing actually runs
RUN /usr/local/bin/micropython -h

ENTRYPOINT ["/usr/local/bin/micropython"]
